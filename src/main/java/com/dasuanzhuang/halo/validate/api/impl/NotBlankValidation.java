package com.dasuanzhuang.halo.validate.api.impl;

import com.dasuanzhuang.halo.validate.api.Validation;

public class NotBlankValidation implements Validation {
    @Override
    public Boolean validate(Object attrValue, Class<?> attrType, Object params) {
        if (attrType.equals(String.class)) {
            String value = (String) attrValue;
            return value != null && value.trim().length() != 0;
        }
        return false;
    }
}
