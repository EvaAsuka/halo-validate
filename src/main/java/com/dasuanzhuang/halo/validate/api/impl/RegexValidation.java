package com.dasuanzhuang.halo.validate.api.impl;

import com.dasuanzhuang.halo.validate.api.Validation;

import java.util.regex.Pattern;

public class RegexValidation implements Validation {
    @Override
    public Boolean validate(Object attrValue, Class<?> attrType, Object params) {
        if (attrValue == null) {
            return true;
        }
        String regex = (String) params;
        Pattern pattern = Pattern.compile(regex);
        if (attrType.equals(String.class)) {
            return pattern.matcher((String) attrValue).matches();
        }
        return false;
    }
}
