package com.dasuanzhuang.halo.validate.api.impl;

import com.dasuanzhuang.halo.validate.api.Validation;

public class NotNullValidation implements Validation {
    @Override
    public Boolean validate(Object attrValue, Class<?> attrType, Object params) {
        return attrValue != null;
    }
}
